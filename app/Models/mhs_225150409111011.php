<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mhs_225150409111011 extends Model
{
    use HasFactory;

    public $table = "mhs_225150409111011";

    protected $fillable = [
        'NIM', 'nama'
    ];
}
