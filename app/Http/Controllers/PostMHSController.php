<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\mhs_225150409111011;

class PostMHSController extends Controller
{
    public function index()
    {
        $posts = mhs_225150409111011::latest()->get();
        return view('posts.index', compact('posts'));
    }
    
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'NIM' => 'required',
            'nama' => 'required',
        ]);

        $post = mhs_225150409111011::create([
            'NIM' => $request->NIM,
            'nama' => $request->nama
        ]);

        if ($post) {
            return redirect()
                ->route('post.index')
                ->with([
                    'success' => 'New post has been created successfully'
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    'error' => 'Some problem occurred, please try again'
                ]);
        }
    }
}
